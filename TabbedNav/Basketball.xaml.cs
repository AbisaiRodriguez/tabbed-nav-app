﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace TabbedNav
{
    public partial class Basketball : ContentPage
    {
        public Basketball()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Basketball)}:  ctor");

        }

        void OnAppearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}:  ctor");

        
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}:  ctor");

        }
    }
}
