﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace TabbedNav
{
    public partial class Soccer : ContentPage
    {
        public Soccer()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Soccer)}:  ctor");

        }

        void OnAppearing(object sender, System.EventArgs e)
        {
            
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");

        }

        void OnDisappearing(object sender, System.EventArgs e)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }
    }
}
