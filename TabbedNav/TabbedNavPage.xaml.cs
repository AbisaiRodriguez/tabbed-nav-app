﻿using Xamarin.Forms;
using System.Diagnostics;

namespace TabbedNav
{
    public partial class TabbedNavPage : TabbedPage
    {
        public TabbedNavPage()
        {
            InitializeComponent();
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(TabbedNavPage)}:  ctor");

        }

        void OnAppearing(object sender, System.EventArgs e)
        {

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnAppearing)}");
        }

        void OnDisappearing(object sender, System.EventArgs e)
        {

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnDisappearing)}");
        }

    }
}
